package com.quangsang.jpastremerservice.controller;

import com.quangsang.jpastremerservice.dto.UsersDTO;
import com.quangsang.jpastremerservice.model.UsersEntity;
import com.quangsang.jpastremerservice.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/test")
public class TestController {

    private final UserService userService;

    public TestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public UsersDTO createUser(String name,Long userId){
        UsersDTO dto = userService.createUser("sang",1L);
        return dto;
    }

    @GetMapping("/{id}")
    public UsersEntity ping(@PathVariable("id") Long id) {
        UsersEntity usersEntity = userService.getUserById(id);
        return usersEntity;
    }

    @GetMapping("/user1")
    public List<UsersDTO> getUsers(@RequestParam Long offset, @RequestParam Long limit) {
        return userService.getUsers(offset, limit);
    }

    @GetMapping("/user2")
    public List<UsersEntity> getUsersByDept(@RequestParam String address) {
        return userService.getUsersByDept(address);
    }

    @GetMapping("/user3")
    public List<UsersEntity> getUsersByAddressAndPhone(@RequestParam String address, @RequestParam Long phone) {
        return userService.getUsersByAddressAndPhone(address, phone);
    }

    @GetMapping("/min")
    public UsersEntity minPaidEmp() {
        return userService.minPaidEmp();
    }

    @GetMapping("/ids")
    public List<UsersEntity> getUsersByIds(@RequestBody List<Long> ids) {
        return userService.getUsersByIds(ids);
    }

    @GetMapping("/groupByAddress")
    public Map<String, List<UsersEntity>> getUserGroupByAddress() {
        return userService.getUserGroupByAddress();
    }
}
