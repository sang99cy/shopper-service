package com.quangsang.jpastremerservice.controller;

import com.quangsang.jpastremerservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/zeppelin/")
public class ZeppelinController {

    @Autowired
    private UserService userService;

    @GetMapping
    public String cloneNoteBook(){
        String message;
        String JessionId = userService.login();
        System.out.println("JessionId:"+JessionId);
        if(JessionId.length() > 0){
            userService.cloneNoteBook();
            message = "clone noteBook thành công";
        }else {
            message = "clone note book thất bại";
        }
        return message;
    }
}
