package com.quangsang.jpastremerservice.repository.Impl;

import com.quangsang.jpastremerservice.model.UsersEntity;
import com.quangsang.jpastremerservice.repository.UserRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

class UserRepositoryImpl implements UserRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void refresh(UsersEntity usersEntity) {
        em.refresh(usersEntity);
    }
}
