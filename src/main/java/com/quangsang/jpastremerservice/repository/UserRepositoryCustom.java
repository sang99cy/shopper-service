package com.quangsang.jpastremerservice.repository;

import com.quangsang.jpastremerservice.model.UsersEntity;

public interface UserRepositoryCustom {
    void refresh(UsersEntity usersEntity);
}
