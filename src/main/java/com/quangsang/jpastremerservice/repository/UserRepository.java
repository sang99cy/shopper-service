package com.quangsang.jpastremerservice.repository;

import com.quangsang.jpastremerservice.model.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UsersEntity, Long>, UserRepositoryCustom,JpaSpecificationExecutor<UsersEntity> {
}
