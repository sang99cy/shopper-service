package com.quangsang.jpastremerservice.config;

import com.quangsang.jpastremerservice.service.MapService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerConfig {

    @Bean
    public ModelMapper modelMapper() {
        // Tạo object và cấu hình
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT) /**/
                .setSkipNullEnabled(true) /*có cho phép bỏ qua thuộc tính null hay không*/
                .setDeepCopyEnabled(true) /*mặc định dùng shallow copy, dùng deep copy thì sẽ chậm hơn.*/
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PUBLIC)  /*chỉ định field truy cập ở mức độ nào (private, public,...)*/
                .setMethodAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PUBLIC) /*chỉ định mức độ truy cập method, getter và setter*/
//                .setSourceNameTokenizer((s, nameableType) -> {})/*chỉ định quy ước đặt tên cho thuộc tính ở source (object nguồn dùng để map)*/
//                .setDestinationNameTokenizer() /*chỉ định quy ước đặt tên cho thuộc tính ở đích (object map ra).*/
        ;
        return modelMapper;
    }

    @Bean
    public MapService mapService() {
        MapService mapService = new MapService(modelMapper());
        return mapService;
    }
}
