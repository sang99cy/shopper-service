package com.quangsang.jpastremerservice.service.Impl;

import com.quangsang.jpastremerservice.annotation.JsessionZeppelin;
import com.quangsang.jpastremerservice.annotation.TrackExecutionTime;
import com.quangsang.jpastremerservice.dto.UsersDTO;
import com.quangsang.jpastremerservice.model.UsersEntity;
import com.quangsang.jpastremerservice.model.UsersEntity$;
import com.quangsang.jpastremerservice.repository.UserRepository;
import com.quangsang.jpastremerservice.service.MapService;
import com.quangsang.jpastremerservice.service.UserService;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final JPAStreamer jpaStreamer;
    private final MapService mapService;

    public UserServiceImpl(UserRepository userRepository, JPAStreamer jpaStreamer, MapService mapService) {
        this.userRepository = userRepository;
        this.jpaStreamer = jpaStreamer;
        this.mapService = mapService;
    }

    @Override
    public void createUser(UsersEntity user) {
        userRepository.save(user);
    }

    @Override
    @TrackExecutionTime
    public UsersEntity getUserById(Long id) {
        UsersEntity usersEntity = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("not found user"));
        return usersEntity;
    }

    @Override
    public List<UsersDTO> getUsers(Long offset, Long limit) {
        List<UsersEntity> list = jpaStreamer.stream(UsersEntity.class)
                .sorted(UsersEntity$.userName)
                .skip(offset).limit(limit)
                .collect(Collectors.toList());
        return mapService.mapList(list,UsersDTO.class);
    }

    @Override
    public List<UsersEntity> getUsersByDept(String address) {
        return jpaStreamer.stream(UsersEntity.class)
                .filter(UsersEntity$.address.equal(address))
                .collect(Collectors.toList());
    }

    @Override
    public List<UsersEntity> getUsersByAddressAndPhone(String address, Long phone) {
        return jpaStreamer.stream(UsersEntity.class)
                .filter(UsersEntity$.address.equal(address).and(UsersEntity$.phoneNumber.greaterOrEqual(phone)))
                .collect(Collectors.toList());
    }

    @Override
    public UsersEntity minPaidEmp() {
        return jpaStreamer.stream(UsersEntity.class)
                .min(Comparator.comparing(UsersEntity::getPhoneNumber)).get();
    }

    @Override
    public List<UsersEntity> getUsersByIds(List<Long> ids) {
        return jpaStreamer.stream(UsersEntity.class)
                .filter(UsersEntity$.userId.in(ids))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<UsersEntity>> getUserGroupByAddress() {
        return jpaStreamer.stream(UsersEntity.class)
                .collect(Collectors.groupingBy(UsersEntity::getAddress));
    }

    @Override
    public UsersDTO createUser(String name, Long userId) {
        UsersDTO dto = new UsersDTO();
        dto.setUserName(name);
        dto.setUserId(userId);
        return dto;
    }

    @Override
    @JsessionZeppelin
    public String login() {
        return "làm login lấy sesionID";
    }

    @Override
    public void cloneNoteBook() {
        System.out.println("ham xu ly clone note book");
    }
}
