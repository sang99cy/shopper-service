package com.quangsang.jpastremerservice.service;

import com.quangsang.jpastremerservice.dto.BaseDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MapService {

    @Autowired
    private ModelMapper modelMapper;

    public MapService() {
    }

    public MapService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public <T, E> T map(E entity, Class<T> clazz) {
        return modelMapper.map(entity, clazz);
    }

    public <T, E extends BaseDTO> T map(E dto, Class<T> clazz) {
        return modelMapper.map(dto, clazz);
    }

    public <T, E> void mapData(T source, E destination) {
        modelMapper.map(source, destination);
    }

    public <T, E> List<T> mapList(List<E> inputData, Class<T> clazz) {
        return CollectionUtils.isEmpty(inputData) ?
                Collections.emptyList() :
                inputData.stream()
                        .map(i -> modelMapper.map(i, clazz))
                        .collect(Collectors.toList());
    }
}
