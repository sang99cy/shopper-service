package com.quangsang.jpastremerservice.service;

import com.quangsang.jpastremerservice.dto.UsersDTO;
import com.quangsang.jpastremerservice.model.UsersEntity;

import java.util.List;
import java.util.Map;

public interface UserService {
    void createUser(UsersEntity user);
    UsersEntity getUserById(Long id);
    List<UsersDTO> getUsers(Long offset, Long limit);
    List<UsersEntity> getUsersByDept(String address);
    List<UsersEntity> getUsersByAddressAndPhone(String address,Long phone);
    UsersEntity minPaidEmp();
    List<UsersEntity> getUsersByIds(List<Long> ids);
    Map<String, List<UsersEntity>> getUserGroupByAddress();

    UsersDTO createUser(String name,Long userId);
    String login();
    void cloneNoteBook();
}
