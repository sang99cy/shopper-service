package com.quangsang.jpastremerservice.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InformationDTO {
    String url;
    String username;
    String password;
}
