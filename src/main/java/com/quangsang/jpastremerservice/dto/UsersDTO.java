package com.quangsang.jpastremerservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UsersDTO extends BaseDTO {
    private long userId;
    private String firstName;
    private String lastName;
    private String fullName;
    private String userName;
    private String passWord;
    private String address;
    private String email;
    private Long phoneNumber;
}
