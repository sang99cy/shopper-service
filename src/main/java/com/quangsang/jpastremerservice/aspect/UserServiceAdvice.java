package com.quangsang.jpastremerservice.aspect;

import com.quangsang.jpastremerservice.dto.InformationDTO;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UserServiceAdvice {

    /*các phương thức nào có 2 tham số đều được trỏ đến vs expression*/
    @Before(value = "execution(* com.quangsang.jpastremerservice.service.UserService.*(..)) && args(name,userId)")
    public void beforeAdvice(JoinPoint joinPoint, String name, Long userId) {
        System.out.println("Before method:" + joinPoint.getSignature());

        System.out.println("Creating User with name - " + name + " and id - " + userId);
    }

    @Around(value = "@annotation(com.quangsang.jpastremerservice.annotation.JsessionZeppelin)")
    public String GetJsessionId(ProceedingJoinPoint pjp) throws Throwable {
        pjp.proceed();
        InformationDTO dto = new InformationDTO();
        dto.setUrl("sang");
        dto.setUsername("sang");
        dto.setPassword("sang");
        String jessionId = dto.getUrl() +"/"+dto.getUsername()+"/"+dto.getPassword();
        return jessionId;
    }

    @After(value = "execution(* com.quangsang.jpastremerservice.service.UserService.*(..)) && args(name,userId)")
    public void afterAdvice(JoinPoint joinPoint, String name, Long userId) {
        System.out.println("After method:" + joinPoint.getSignature());

        System.out.println("Successfully created User with name - " + name + " and id - " + userId);
    }
}
