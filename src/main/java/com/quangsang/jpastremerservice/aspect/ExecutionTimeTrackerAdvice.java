package com.quangsang.jpastremerservice.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class ExecutionTimeTrackerAdvice {

    @Around("@annotation(com.quangsang.jpastremerservice.annotation.TrackExecutionTime)")
    public Object trackTime(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object obj =  pjp.proceed();
        long endTime =  System.currentTimeMillis();
        System.out.println("Method name"+pjp.getSignature()+" time taken to excute: "+(endTime-startTime));
        return obj;
    }
}
